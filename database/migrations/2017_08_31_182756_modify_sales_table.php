<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifySalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function(Blueprint $table)
        {
            $table->dropColumn('sale_type');
            $table->dropColumn('trip_mileage');
            $table->dropColumn('cost_per_mile');
            $table->dropColumn('delivery_cost');
            $table->dropColumn('graphics');
            $table->dropColumn('training_date');
            $table->dropColumn('training_location');
            $table->dropColumn('training_cost');
            $table->dropColumn('payment_options');
            $table->dropColumn('sale_date');
            $table->dropColumn('completion_delivery_date');
        });

        Schema::table('sales', function (Blueprint $table) {
            $table->string('sale_type');
            $table->string('trip_mileage')->nullable();
            $table->string('cost_per_mile')->nullable();
            $table->string('delivery_cost')->nullable();
            $table->string('graphics')->nullable();
            $table->string('training_date')->nullable();
            $table->string('training_location')->nullable();
            $table->string('training_cost')->nullable();
            $table->string('payment_options');
            $table->string('sale_date');
            $table->string('completion_delivery_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
