<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->enum('form_of_enquiry',array('telephone','online','social_media','mail_merge','other_marketing','already_customer','word_of_mouth'));
            $table->integer('user_id');
            $table->enum('nature_of_sale',array('tower','support_services','installation'));
            $table->enum('tower_options',array('v-ceptor_ptz','v-ceptor_ip','thermal','v-fire','other'));
            $table->string('other_tower')->nullable();
            $table->date('quote_date');
            $table->float('quote_amount');
            $table->boolean('order_placed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
