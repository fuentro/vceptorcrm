<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quote_id');
            $table->integer('client_id');
            $table->integer('user_id');
            $table->float('equipment_cost');
            $table->string('delivery_address');
            $table->enum('sale_type',array('deploy','delivered','monitor','maintain','installation'));
            $table->enum('payment_options',array('cash','50%','leasing'));
            $table->string('email_record');
            $table->date('sale_date');
            $table->date('completion_delivery_date');
            $table->enum('collection_delivery',array('collection','delivery'));
            $table->integer('trip_mileage');
            $table->integer('cost_per_mile');
            $table->float('delivery_cost');
            $table->string('ral_colour');
            $table->string('graphics');
            $table->enum('sim_card_received',array('yes','no'));
            $table->enum('training_required',array('yes','no'));
            $table->date('training_date');
            $table->enum('training_location',array('away','workshop'));
            $table->float('training_cost');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
