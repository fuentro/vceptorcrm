<h1>Sale Reminder</h1>
This is a <b>reminder</b> that the Sale Number <b>{{$sale->id}}</b> will be due to complete in 7 days. Please follow up to avoid any delays in this order. <br>

<h1>Sale Details</h1>
<br>

<b>ID</b> :  {!! $sale->id !!} <br>
<b>Quote ID</b> : {{$sale->quote_id}} <br>
<b>Client ID</b> : {!! $sale->client_id !!} <br>
<b>Sales Representative</b> {!! $sale->user_id !!} <br>
<b>Equipment Cost</b> : {!! $sale->equipment_cost !!} <br>
<b>Delivery Address</b> : {!! $sale->delivery_address !!} <br>
<b>Sale Type</b> : {!! implode(',',$sale->sale_type) !!} <br>
<b>Payment Options</b> : {!! $sale->payment_options !!} <br>
<b>Email Record</b> : {!! $sale->email_record !!} <br>
<b>Sale Date</b> : {!! $sale->sale_date !!} <br>
<b>Completion & Delivery Date</b> : {!! $sale->completion_delivery_date !!} <br>
<b>Collection / Delivery</b> : {!! $sale->collection_delivery !!} <br>
<b>Trip Mileage</b> : {!! $sale->trip_mileage !!} <br>
<b>Cost Per Mile</b> : {!! $sale->cost_per_mile !!} <br>
<b>Deivery Cost</b> : {!! $sale->delivery_cost !!} <br>
<b>Ral Colour</b> : {!! $sale->ral_colour !!} <br>
<b>Sim Card Received</b> : {!! $sale->sim_card_received !!} <br>
<b>Training Required</b> : {!! $sale->training_required !!} <br>
<b>Date Booked</b> : {!! $sale->training_date !!} <br>
<b>Training Required</b> : {!! $sale->training_location !!} <br>
<b>Training Cost</b> : {!! $sale->training_cost !!} <br>


