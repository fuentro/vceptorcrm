@extends ('layouts.admin')

@section('content')
<div class="inner-wrapper">
                <!-- start: sidebar -->
                <aside id="sidebar-left" class="sidebar-left">
                
                    <div class="sidebar-header">
                        <div class="sidebar-title">
                            Navigation
                        </div>
                        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                        </div>
                    </div>
                
                    <div class="nano">
                        <div class="nano-content">
                            <nav id="menu" class="nav-main" role="navigation">
                            
                                <ul class="nav nav-main">
                                    <li>
                                        <a href="/dashboard">
                                            <i class="fa fa-home" aria-hidden="true"></i>
                                            <span>Dashboard</span>
                                        </a>                        
                                    </li>
                                    <li class="nav-parent nav-expanded nav-active">
                                        <a href="#">
                                            <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                            <span>Clients</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li class="nav-active">
                                                <a href="/clients/create">
                                                    Enter New Client
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/clients/all">
                                                    View All Clients
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </li>
                                    <li class="nav-parent">
                                        <a href="/clients/all">
                                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                            <span>Quotes</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="/quotes/create">
                                                    Enter New Quote
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/quotes/all">
                                                    View All Quotes
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </li>
                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                            <span>Sales</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="/sales/create">
                                                    Enter New Sale
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/sales">
                                                    View All Sales
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </li>
                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="fa fa-id-badge" aria-hidden="true"></i>
                                            <span>Users</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="/users/create">
                                                    Enter New User
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/users">
                                                    View All Users
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </li>                               
                
                                <script>
                                    // Maintain Scroll Position
                                    if (typeof localStorage !== 'undefined') {
                                        if (localStorage.getItem('sidebar-left-position') !== null) {
                                            var initialPosition = localStorage.getItem('sidebar-left-position'),
                                                sidebarLeft = document.querySelector('#sidebar-left .nano-content');
                                            
                                            sidebarLeft.scrollTop = initialPosition;
                                        }
                                    }
                                </script>
                            </nav>
                        </div>
                    </div> 
                </aside>
                <!-- end: sidebar -->

                <section role="main" class="content-body">
                    <header class="page-header">
                        <h2>Enter New Client</h2>
                    
                        <div class="right-wrapper pull-right">
                            <ol class="breadcrumbs">
                                <li>
                                    <a href="/dashboard">
                                        <i class="fa fa-home"></i>
                                    </a>
                                    <li><span>Clients</span></li>
                                    <li><span>Enter New Client</span></li>
                                </li>
                            </ol>
                    
                            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
                        </div>
                    </header>

                    <!-- start: page -->
                    <div class="col-lg-6">
                        <form role="form" method="post">

                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                    <p class="alert alert-danger">{!! $error !!}</p>
                                @endforeach
                            @endif

                            @if (session('status'))
                                <div class="alert alert-success">
                                {{ session('status') }}
                                </div>
                            @endif

                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">                    
                                 
                                <label>Full Name</label>
                                <input class="form-control" name="full_name" id="full_name">
                                <label>Company Name</label>
                                <input class="form-control" name="company_name" id="company_name">
                                <label>Billing Address</label>
                                <input class="form-control" name="billing_address" id="billing_address">
                                <div class="checkbox">
                                    <label>
                                        <input type="hidden" name="delivery_same_as_billing" value=0 />
                                        <input type="checkbox" value=1 name="delivery_same_as_billing" onclick="CopyDeliveryAddress(this.form)" id="delivery_same_as_billing">Check this box if your billing and delivery address are the same.
                                    </label>
                                </div>
                                <label>Delivery Address</label>
                                <input class="form-control" name="delivery_address" id="delivery_address">
                                <label>Email Address</label>
                                <input class="form-control" name="email" id="email">
                                <label>Telephone Number</label>
                                <input class="form-control" name="telephone_number" id="telephone_number">
                                <label>Accounts Contact</label>
                                <input class="form-control" name="accounts_contact" id="accounts_contact">
                                <label>Sales Representative</label>
                                <select class="form-control" name="user_id" id="user_id">
                                        <option value=""></option>
                                        @foreach ($users as $user)                        
                                            <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                        @endforeach
                                </select>
                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea class="form-control" rows="3" name="notes" id="notes"></textarea>
                                </div></br>
                                <button type="submit" class="btn btn-success">Submit</button>                                
                        </form>                            
                    </div>
                    <!-- end: page -->



@endsection

@section('footer')

<script>
    $("#delivery_same_as_billing").on('change', function() {
        if (this.checked) {
            $("#delivery_address").val($("#billing_address").val());                        
        } else {
            $("#delivery_address").val("");
        }
    }); 
</script>

@endsection


