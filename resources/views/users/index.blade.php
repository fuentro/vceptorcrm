@extends ('layouts.admin')

@section('content')
    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <aside id="sidebar-left" class="sidebar-left">

            <div class="sidebar-header">
                <div class="sidebar-title">
                    Navigation
                </div>
                <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>

            <div class="nano">
                <div class="nano-content">
                    <nav id="menu" class="nav-main" role="navigation">

                        <ul class="nav nav-main">
                            <li>
                                <a href="/dashboard">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                    <span>Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-parent">
                                <a href="#">
                                    <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                    <span>Clients</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                        <a href="/clients/create">
                                            Enter New Client
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/clients/all">
                                            View All Clients
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent  ">
                                <a href="#">
                                    <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                    <span>Quotes</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                        <a href="/quotes/create">
                                            Enter New Quote
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/quotes/all">
                                            View All Quotes
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent">
                                <a href="#">
                                    <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                    <span>Sales</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                        <a href="/sales/create">
                                            Enter New Sale
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/sales">
                                            View All Sales
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent nav-active nav-expanded">
                                <a href="#">
                                    <i class="fa fa-id-badge" aria-hidden="true"></i>
                                    <span>Users</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                        <a href="/users/create">
                                            Enter New User
                                        </a>
                                    </li>
                                    <li class="nav-active">
                                        <a href="/users">
                                            View All Users
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <script>
                                // Maintain Scroll Position
                                if (typeof localStorage !== 'undefined') {
                                    if (localStorage.getItem('sidebar-left-position') !== null) {
                                        var initialPosition = localStorage.getItem('sidebar-left-position'),
                                                sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                                        sidebarLeft.scrollTop = initialPosition;
                                    }
                                }
                            </script>
                    </nav>
                </div>
            </div>
        </aside>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
            <header class="page-header">
                <h2>View All Users</h2>

                <div class="right-wrapper pull-right">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="/dashboard">
                                <i class="fa fa-home"></i>
                            </a>
                        <li><span>Users</span></li>
                        <li><span>View All Users</span></li>
                        </li>
                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none" id="datatable-users">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>

                            <td>{!! $user->id !!}</td>
                            <td>{!! $user->name !!}</td>
                            <td>{!! $user->email !!}</td>

                            <td>
                                <a href="{!! route('users.edit',$user->id) !!}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Update</a>

                                <form method="POST" id="delete-{{$user->id}}"  action="{{route('users.destroy',$user->id)}}">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}

                                </form>
                                <a href="#" onclick="$('#delete-'+{{$user->id}}).submit()" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove-sign"></i> Delete</a>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end: page -->



            @endsection

            @section('footer')
                <script>
                    $(document).ready(function(){
                        $('#datatable-users').DataTable();
                    });
                </script>
@endsection


