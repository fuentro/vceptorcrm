@extends ('layouts.admin')

@section('content')
    <div class="inner-wrapper">
        <aside id="sidebar-left" class="sidebar-left">

            <div class="sidebar-header">
                <div class="sidebar-title">
                    Navigation
                </div>
                <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>

            <div class="nano">
                <div class="nano-content">
                    <nav id="menu" class="nav-main" role="navigation">

                        <ul class="nav nav-main">
                            <li>
                                <a href="/dashboard">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                    <span>Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-parent">
                                <a href="#">
                                    <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                    <span>Clients</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                        <a href="/clients/create">
                                            Enter New Client
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/clients/all">
                                            View All Clients
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent ">
                                <a href="#">
                                    <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                    <span>Quotes</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li  >
                                        <a href="/quotes/create">
                                            Enter New Quote
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/quotes/all">
                                            View All Quotes
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent">
                                <a href="#">
                                    <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                    <span>Sales</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                        <a href="/sales/create">
                                            Enter New Sale
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/sales">
                                            View All Sales
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent nav-expanded nav-active">
                                <a href="#">
                                    <i class="fa fa-id-badge" aria-hidden="true"></i>
                                    <span>Users</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li class="nav-active">
                                        <a href="/users/create">
                                            Enter New User
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/users">
                                            View All Users
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <script>
                                // Maintain Scroll Position
                                if (typeof localStorage !== 'undefined') {
                                    if (localStorage.getItem('sidebar-left-position') !== null) {
                                        var initialPosition = localStorage.getItem('sidebar-left-position'),
                                                sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                                        sidebarLeft.scrollTop = initialPosition;
                                    }
                                }
                            </script>
                    </nav>
                </div>
            </div>
        </aside>

        <section role="main" class="content-body">
            <header class="page-header">
                <h2>Enter New User</h2>

                <div class="right-wrapper pull-right">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="/dashboard">
                                <i class="fa fa-home"></i>
                            </a>
                        <li><span>Users</span></li>
                        <li><span>Enter New User</span></li>
                        </li>
                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
                </div>
            </header>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Create</div>

                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('users.store') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-4 control-label">Name</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Password</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Create
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


</div>
@endsection
