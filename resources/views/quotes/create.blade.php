@extends ('layouts.admin')

@section('content')
<div class="inner-wrapper">
                <!-- start: sidebar -->
                <aside id="sidebar-left" class="sidebar-left">
                
                    <div class="sidebar-header">
                        <div class="sidebar-title">
                            Navigation
                        </div>
                        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                        </div>
                    </div>
                
                    <div class="nano">
                        <div class="nano-content">
                            <nav id="menu" class="nav-main" role="navigation">
                            
                                <ul class="nav nav-main">
                                    <li>
                                        <a href="/dashboard">
                                            <i class="fa fa-home" aria-hidden="true"></i>
                                            <span>Dashboard</span>
                                        </a>                        
                                    </li>
                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                            <span>Clients</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="/clients/create">
                                                    Enter New Client
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/clients/all">
                                                    View All Clients
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </li>
                                    <li class="nav-parent nav-expanded nav-active">
                                        <a href="#">
                                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                            <span>Quotes</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li  class="nav-active">
                                                <a href="/quotes/create">
                                                    Enter New Quote
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/quotes/all">
                                                    View All Quotes
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </li>
                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                            <span>Sales</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="/sales/create">
                                                    Enter New Sale
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/sales">
                                                    View All Sales
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </li>
                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="fa fa-id-badge" aria-hidden="true"></i>
                                            <span>Users</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="/users/create">
                                                    Enter New User
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/users">
                                                    View All Users
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </li>                               
                
                                <script>
                                    // Maintain Scroll Position
                                    if (typeof localStorage !== 'undefined') {
                                        if (localStorage.getItem('sidebar-left-position') !== null) {
                                            var initialPosition = localStorage.getItem('sidebar-left-position'),
                                                sidebarLeft = document.querySelector('#sidebar-left .nano-content');
                                            
                                            sidebarLeft.scrollTop = initialPosition;
                                        }
                                    }
                                </script>
                            </nav>
                        </div>
                    </div> 
                </aside>
                <!-- end: sidebar -->

                <section role="main" class="content-body">
                    <header class="page-header">
                        <h2>Enter New Quote</h2>
                    
                        <div class="right-wrapper pull-right">
                            <ol class="breadcrumbs">
                                <li>
                                    <a href="/dashboard">
                                        <i class="fa fa-home"></i>
                                    </a>
                                    <li><span>Quotes</span></li>
                                    <li><span>Enter New Quote</span></li>
                                </li>
                            </ol>
                    
                            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
                        </div>
                    </header>

                    <!-- start: page -->
                    <div class="col-lg-6">
                        <form id="form" role="form" method="post">
                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                    <p class="alert alert-danger">{!! $error !!}</p>
                                @endforeach
                            @endif
                            @if (session('status'))
                                <div class="alert alert-success">
                                {{ session('status') }}
                                </div>
                            @endif
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                    <div class="form-group">
                                        <label>Client Name</label>
                                        <select class="form-control" name="client_id" id="client_id">
                                            <option value="" ></option>
                                            @foreach ($clients as $client)                        
                                                <option value="{!! $client->id !!}">{!! $client->full_name !!}</option>
                                            @endforeach
                                        </select>
                                        <label>Form of Enquiry</label>
                                        <select class="form-control" name="form_of_enquiry" id="form_of_enquiry">
                                            <option value="" ></option>
                                            <option value="telephone">Telephone</option>                           
                                            <option value="online">Online</option>                            
                                            <option value="social_media">Social Media</option>                            
                                            <option value="mail_merge">Mail Merge</option>                            
                                            <option value="other_marketing">Other Marketing</option>                            
                                            <option value="already_customer">Already Customer</option>                            
                                            <option value="word_of_mouth">Word of Mouth</option>                            
                                        </select>
                                        <label>Sales Representative</label>
                                        <select class="form-control" name="user_id" id="user_id">
                                                <option value="" ></option>
                                                @foreach ($users as $user)                        
                                                    <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                                @endforeach
                                        </select>
                                        <label>Nature Of Sale</label>
                                        <select class="form-control" name="nature_of_sale" id="nature_of_sale">
                                            <option value="" ></option>
                                            <option value="tower">Tower</option>                           
                                            <option value="support_services">Support Services</option>                            
                                            <option value="installation">Installation</option>                           
                                        </select>
                                        <label>Tower Options</label>
                                        <select class="form-control" name="tower_options" id="tower_options" onchange="CheckTower(this.value);">
                                            <option value="" ></option>
                                            <option value="v-ceptor_ptz">V-Ceptor PTZ</option>                           
                                            <option value="v-ceptor_ip">V-Ceptor IP</option>                            
                                            <option value="thermal">Thermal</option>
                                            <option value="v-fire">V-Fire</option>
                                            <option value="other">Other</option>                            
                                        </select>
                                        <input class="form-control" name="other_tower" id="other_tower" placeholder="Please specify Tower Option" style="display:none;">
                                        <label>Quote Date</label>
                                        <input data-plugin-datepicker="" data-plugin-options='{"format": "yyyy/mm/dd"}' class="form-control" name="quote_date" id="quote_date">
                                        <label>Quote Amount</label>
                                        <input class="form-control" type="number" step="any" name="quote_amount" id="quote_amount">
                                        <div class="checkbox">
                                        <label>
                                            <input type="checkbox"   name="order_placed" id="order_placed">Order placed.
                                        </label>
                                        </div>
                                    </div>
                                <button type="submit" class="btn btn-success">Submit</button>
                                <input id="move_to_sale" type="hidden" name="move_to_sale" value="0">
                                <button id="move_to_sale_btn" type="button" class="btn btn-primary">Move to Sale</button>
                            </form>                           
                    </div>
                    <!-- end: page -->



@endsection

@section('footer')

<script type="text/javascript">
    function CheckTower(val){
     var element=document.getElementById('tower_options');
     if(val=='other')
       document.getElementById('other_tower').style.display='block';
     else  
       document.getElementById('other_tower').style.display='none';
    }

    $(document).ready(function(){
        $('#move_to_sale_btn').on('click',function(){
            $('#move_to_sale').val(1);
            $('#form').submit();
        });
    });
</script>

@endsection


