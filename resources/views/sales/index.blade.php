@extends ('layouts.admin')

@section('content')
    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <aside id="sidebar-left" class="sidebar-left">

            <div class="sidebar-header">
                <div class="sidebar-title">
                    Navigation
                </div>
                <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>

            <div class="nano">
                <div class="nano-content">
                    <nav id="menu" class="nav-main" role="navigation">

                        <ul class="nav nav-main">
                            <li>
                                <a href="/dashboard">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                    <span>Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-parent">
                                <a href="#">
                                    <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                    <span>Clients</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                        <a href="/clients/create">
                                            Enter New Client
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/clients/all">
                                            View All Clients
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent ">
                                <a href="#">
                                    <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                    <span>Quotes</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                        <a href="/quotes/create">
                                            Enter New Quote
                                        </a>
                                    </li>
                                    <li >
                                        <a href="/quotes/all">
                                            View All Quotes
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent nav-expanded nav-active">
                                <a href="#">
                                    <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                    <span>Sales</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                        <a href="/sales/create">
                                            Enter New Sale
                                        </a>
                                    </li>
                                    <li class="nav-active">
                                        <a href="/sales">
                                            View All Sales
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent">
                                <a href="#">
                                    <i class="fa fa-id-badge" aria-hidden="true"></i>
                                    <span>Users</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                        <a href="/users/create">
                                            Enter New User
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/users">
                                            View All Users
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <script>
                                // Maintain Scroll Position
                                if (typeof localStorage !== 'undefined') {
                                    if (localStorage.getItem('sidebar-left-position') !== null) {
                                        var initialPosition = localStorage.getItem('sidebar-left-position'),
                                                sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                                        sidebarLeft.scrollTop = initialPosition;
                                    }
                                }
                            </script>
                    </nav>
                </div>
            </div>
        </aside>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
            <header class="page-header">
                <h2>View All Sales</h2>

                <div class="right-wrapper pull-right">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="/dashboard">
                                <i class="fa fa-home"></i>
                            </a>
                        <li><span>Sales</span></li>
                        <li><span>View All Sales</span></li>
                        </li>
                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none" id="datatable-sales">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Quote ID</th>
                        <th>Client ID</th>
                        <th>Sales Representative</th>
                        <th>Status</th>
                        <th>Equipment Cost</th>
                        <th>Delivery Address</th>
                        <th>Sale Type </th>
                        <th>Payment Options</th>
                        <th>Email Record</th>
                        <th>Sale Date</th>
                        <th>Completion &amp; Delivery Date</th>
                        <th>Collection / Delivery</th>
                        <th>Trip Mileage</th>
                        <th>Cost Per Mile</th>
                        <th>Deivery Cost</th>
                        <th>Ral Colour</th>
                        <th>Sim Card Received</th>
                        <th>Training Required</th>
                        <th>Date Booked</th>
                        <th>Training Required</th>
                        <th>Training Cost</th>

                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sales as $sale)
                        <tr>

                            <td>{!! $sale->id !!}</td>
                            <td>{!! $sale->quote_id !!}</td>
                            <td>{!! $sale->client_id !!}</td>
                            <td>{!! $sale->user_id !!}</td>
                            <td style="color: @if($sale->status =='delivered') green; @else red; @endif">{!! $sale->status !!}</td>
                            <td>{!! $sale->equipment_cost !!}</td>
                            <td>{!! $sale->delivery_address !!}</td>
                            <td>{!! implode(',',$sale->sale_type) !!}</td>
                            <td>{!! $sale->payment_options !!}</td>
                            <td>{!! $sale->email_record !!}</td>
                            <td>{!! $sale->sale_date !!}</td>
                            <td>{!! $sale->completion_delivery_date !!}</td>
                            <td>{!! $sale->collection_delivery !!}</td>
                            <td>{!! $sale->trip_mileage !!}</td>
                            <td>{!! $sale->cost_per_mile !!}</td>
                            <td>{!! $sale->delivery_cost !!}</td>
                            <td>{!! $sale->ral_colour !!}</td>
                            <td>{!! $sale->sim_card_received !!}</td>
                            <td>{!! $sale->training_required !!}</td>
                            <td>{!! $sale->training_date !!}</td>
                            <td>{!! $sale->training_location !!}</td>
                            <td>{!! $sale->training_cost !!}</td>

                            <td>
                                <a href="{!! route('sales.edit',$sale->id) !!}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Update</a>

                                <form method="POST" id="delete-{{$sale->id}}"  action="{{route('sales.destroy',$sale->id)}}">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}

                                </form>
                                <a href="#" onclick="$('#delete-'+{{$sale->id}}).submit()" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove-sign"></i> Delete</a>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- end: page -->



            @endsection

            @section('footer')
                <script>
                    $(document).ready(function(){
                        $('#datatable-sales').DataTable();
                    });
                </script>
@endsection


