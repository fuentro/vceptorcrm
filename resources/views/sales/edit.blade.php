@extends ('layouts.admin')

@section('content')
    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <aside id="sidebar-left" class="sidebar-left">

            <div class="sidebar-header">
                <div class="sidebar-title">
                    Navigation
                </div>
                <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>

            <div class="nano">
                <div class="nano-content">
                    <nav id="menu" class="nav-main" role="navigation">

                        <ul class="nav nav-main">
                            <li>
                                <a href="/dashboard">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                    <span>Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-parent">
                                <a href="#">
                                    <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                    <span>Clients</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                        <a href="/clients/create">
                                            Enter New Client
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/clients/all">
                                            View All Clients
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent ">
                                <a href="#">
                                    <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                    <span>Quotes</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li  >
                                        <a href="/quotes/create">
                                            Enter New Quote
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/quotes/all">
                                            View All Quotes
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent nav-expanded nav-active">
                                <a href="#">
                                    <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                    <span>Sales</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li class="nav-active">
                                        <a href="/sales/create">
                                            Enter New Sale
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/sales">
                                            View All Sales
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent ">
                                <a href="#">
                                    <i class="fa fa-id-badge" aria-hidden="true"></i>
                                    <span>Users</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li >
                                        <a href="/users/create">
                                            Enter New User
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/users">
                                            View All Users
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <script>
                                // Maintain Scroll Position
                                if (typeof localStorage !== 'undefined') {
                                    if (localStorage.getItem('sidebar-left-position') !== null) {
                                        var initialPosition = localStorage.getItem('sidebar-left-position'),
                                                sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                                        sidebarLeft.scrollTop = initialPosition;
                                    }
                                }
                            </script>
                    </nav>
                </div>
            </div>
        </aside>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
            <header class="page-header">
                <h2>Edit Sale</h2>

                <div class="right-wrapper pull-right">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="/dashboard">
                                <i class="fa fa-home"></i>
                            </a>
                        <li><span>Sales</span></li>
                        <li><span>Edit Sale</span></li>
                        </li>
                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="col-lg-6">
                <form role="form" method="post" action="{{route('sales.update',$sale->id)}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <p class="alert alert-danger">{!! $error !!}</p>
                        @endforeach
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="form-group">
                        <!-- Quote ID needs to be fed from the Quote model, add a third action
                            button in the view all table? or better handled via the edit quote screen? -->
                        <label>Quote ID</label>
                        <input class="form-control" type="text" name="quote_id" id="quote_id" value="{{old('quote_id',$sale->quote_id)}}">
                        <!-- Client Name to be fed from quotes -->
                        <label>Client Name</label>
                        <select class="form-control" name="client_id" id="client_id">
                            <option value="" ></option>
                            @foreach ($clients as $client)
                                <option address="{{$client->delivery_address}}" value="{!! $client->id !!}" @if($sale->client_id == $client->id) selected @endif>{!! $client->full_name !!}</option>
                            @endforeach
                        </select>
                        <!-- Sales Representative to be fed from quotes -->
                        <label>Sales Representative</label>
                        <select class="form-control" name="user_id" id="user_id">
                            <option value="" ></option>
                            @foreach ($users as $user)
                                <option value="{!! $user->id !!}" @if($sale->user_id == $user->id) selected @endif>{!! $user->name !!}</option>
                            @endforeach
                        </select>
                        <label>Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="in_progress" @if($sale->status == 'in_progress') selected @endif>In Progress</option>
                            <option value="delivered"@if($sale->status == 'delivered') selected @endif>Delivered</option>
                        </select>
                        <label>Equipment Cost</label>
                        <input class="form-control" type="number" step="any" name="equipment_cost" id="equipment_cost" value="{{old('equipment_cost',$sale->equipment_cost)}}">
                        <!-- Delivery address to be fed from client information -->
                        <label>Delivery Address</label>
                        <input class="form-control" type="text" name="delivery_address" id="delivery_address" value="{{old('delivery_address',$sale->delivery_address)}}">
                        <div class="form-group">
                            <label>Sale Type</label>
                            <br>
                            <label class="checkbox-inline"> <input type="checkbox" value="deploy" name="sale_type[]" id="deploy" @if(in_array('deploy',$sale->sale_type)) checked @endif> deploy</label>
                            <label class="checkbox-inline"> <input type="checkbox" value="delivered" name="sale_type[]" id="delivered" @if(in_array('delivered',$sale->sale_type)) checked @endif> delivered </label>
                            <label class="checkbox-inline"> <input type="checkbox" value="monitor" name="sale_type[]" id="monitor" @if(in_array('monitor',$sale->sale_type)) checked @endif> monitor </label>
                            <label class="checkbox-inline"> <input type="checkbox" value="maintain" name="sale_type[]" id="maintain" @if(in_array('maintain',$sale->sale_type)) checked @endif> maintain </label>
                            <label class="checkbox-inline"> <input type="checkbox" value="installation" name="sale_type[]" id="installation" @if(in_array('installation',$sale->sale_type)) checked @endif> installation </label>
                        </div>
                        <div class="form-group">
                            <label>Payment Options</label>
                            <select class="form-control" name="payment_options" id="payment_options">
                                <option value="cash" @if($sale->payment_option == 'cash') selected @endif>Cash</option>
                                <option value="50%"@if($sale->payment_option == '50%') selected @endif>50%</option>
                                <option value="deposit"@if($sale->payment_option == 'deposit') selected @endif>Deposit</option>
                                <option value="leasing"@if($sale->payment_option == 'leasing') selected @endif>Leasing</option>
                            </select>
                        </div>

                        <label>Email Record</label>
                        <input class="form-control" type="text" name="email_record" id="email_record" value="{{old('email_record',$sale->email_record)}}">
                        <label>Sale Date</label>
                        <input data-plugin-datepicker="" data-plugin-options='{"format": "yyyy/mm/dd"}' class="form-control" name="sale_date" id="sale_date" value="{{old('sale_date',$sale->sale_date)}}">
                        <!-- Automatically chooses date 28 days after order date -->
                        <label>Completion &amp; Delivery Date</label>
                        <input data-plugin-datepicker="" data-plugin-options='{"format": "yyyy/mm/dd"}' class="form-control" name="completion_delivery_date" id="completion_delivery_date" value="{{old('completion_delivery_date',$sale->completion_delivery_date)}}" readonly>
                        <label>Collection / Delivery</label>
                        <select class="form-control" name="collection_delivery" id="collection_delivery">
                            <option value="collection" @if($sale->collection_delivery == 'collection') selected @endif>Collection</option>
                            <option value="delivery"@if($sale->collection_delivery == 'delivery') selected @endif>Delivery</option>
                        </select>
                        <!-- If Delivery is selected then next 3 fields should enable
                                'trip_mileage', 'cost_per_mile', 'delivery_cost'
                                Delivery Cost is calculated: trip mileage * cost per mile -->
                        <label>Trip Mileage</label>
                        <input  class="form-control delivery" type="number" step="any" name="trip_mileage" id="trip_mileage" @if($sale->trip_mileage && $sale->collection_delivery == 'delivery' ) value="{{$sale->trip_mileage}}" @else readonly @endif>
                        <label>Cost Per Mile</label>
                        <input class="form-control delivery" type="number" step="any" name="cost_per_mile" id="cost_per_mile" @if($sale->cost_per_mile && $sale->collection_delivery == 'delivery' ) value="{{$sale->cost_per_mile}}" @else readonly @endif>
                        <label>Deivery Cost</label>
                        <input class="form-control" type="number" step="any" name="delivery_cost" id="delivery_cost" @if($sale->delivery_cost && $sale->collection_delivery == 'delivery' ) value="{{$sale->delivery_cost}}"   @endif readonly>
                        <label>Ral Colour</label>
                        <input class="form-control" type="text" name="ral_colour" id="ral_colour" value="{{old('ral_colour',$sale->ral_colour)}}">


                        <!-- Here needs an additional field for uploading/handling images to upload -->
                        <label>Graphics</label>
                        @if(!is_null($sale->graphicsAssetPath())) <img width="100" height="100" style=" border-radius: 50%" src="{{$sale->graphicsAssetPath()}}"> @endif
                        <input type="file" class="form-control" name="graphics">

                        <label>Sim Card Received</label>
                        <select class="form-control" name="sim_card_received" id="sim_card_received">
                            <option value="yes" @if($sale->sim_card_received == 'yes') selected @endif>YES</option>
                            <option value="no" @if($sale->sim_card_received == 'no') selected @endif>NO</option>
                        </select>
                        <label>Training Required</label>
                        <select class="form-control" name="training_required" id="training_required">
                            <option value="yes" @if($sale->training_required == 'yes') selected @endif>YES</option>
                            <option value="no"@if($sale->training_required == 'no') selected @endif>NO</option>
                        </select>

                        <!--If 'YES' is selected from training required then the next 3 fields need to enable -->

                        <label>Date Booked</label>
                        <input data-plugin-datepicker="" data-plugin-options='{"format": "yyyy/mm/dd"}' class="form-control training" name="training_date" id="training_date" @if($sale->training_date && $sale->training_required == 'yes') value="{{$sale->training_date}}" @else disabled @endif>
                        <label>Training Required</label>

                        <select class="form-control training" name="training_location" id="training_location" @if( !($sale->training_required == 'yes')) disabled @endif>
                            <option value=""></option>
                            <option value="away" @if($sale->training_location =='away' && $sale->training_required == 'yes') selected @endif>Away</option>
                            <option value="workshop" @if($sale->training_location =='workshop' && $sale->training_required == 'yes') selected @endif>Workshop</option>
                        </select>
                        <label>Training Cost</label>
                        <input class="form-control training" type="number" step="any" name="training_cost" id="training_cost"  @if($sale->training_cost && $sale->training_required == 'yes') value="{{$sale->training_cost}}" @else disabled @endif>
                        <br>
                    </div>
                    <button type="submit" class="btn btn-success">Update</button>
                </form>
            </div>
            <!-- end: page -->



@endsection

@section('footer')
    @include('sales.scripts')
@endsection


