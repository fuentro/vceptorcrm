@extends ('layouts.admin')

@section('content')
<div class="inner-wrapper">
                <!-- start: sidebar -->
    <aside id="sidebar-left" class="sidebar-left">

        <div class="sidebar-header">
            <div class="sidebar-title">
                Navigation
            </div>
            <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <div class="nano">
            <div class="nano-content">
                <nav id="menu" class="nav-main" role="navigation">

                    <ul class="nav nav-main">
                        <li>
                            <a href="/dashboard">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-parent">
                            <a href="#">
                                <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                <span>Clients</span>
                            </a>
                            <ul class="nav nav-children">
                                <li>
                                    <a href="/clients/create">
                                        Enter New Client
                                    </a>
                                </li>
                                <li>
                                    <a href="/clients/all">
                                        View All Clients
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-parent ">
                            <a href="#">
                                <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                <span>Quotes</span>
                            </a>
                            <ul class="nav nav-children">
                                <li  >
                                    <a href="/quotes/create">
                                        Enter New Quote
                                    </a>
                                </li>
                                <li>
                                    <a href="/quotes/all">
                                        View All Quotes
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-parent nav-expanded nav-active">
                            <a href="#">
                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                <span>Sales</span>
                            </a>
                            <ul class="nav nav-children">
                                <li class="nav-active">
                                    <a href="/sales/create">
                                        Enter New Sale
                                    </a>
                                </li>
                                <li>
                                    <a href="/sales">
                                        View All Sales
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-parent ">
                            <a href="#">
                                <i class="fa fa-id-badge" aria-hidden="true"></i>
                                <span>Users</span>
                            </a>
                            <ul class="nav nav-children">
                                <li >
                                    <a href="/users/create">
                                        Enter New User
                                    </a>
                                </li>
                                <li>
                                    <a href="/users">
                                        View All Users
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <script>
                            // Maintain Scroll Position
                            if (typeof localStorage !== 'undefined') {
                                if (localStorage.getItem('sidebar-left-position') !== null) {
                                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                                            sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                                    sidebarLeft.scrollTop = initialPosition;
                                }
                            }
                        </script>
                </nav>
            </div>
        </div>
    </aside>
                <!-- end: sidebar -->

                <section role="main" class="content-body">
                    <header class="page-header">
                        <h2>Enter New Sale</h2>
                    
                        <div class="right-wrapper pull-right">
                            <ol class="breadcrumbs">
                                <li>
                                    <a href="/dashboard">
                                        <i class="fa fa-home"></i>
                                    </a>
                                    <li><span>Sales</span></li>
                                    <li><span>Enter New Sale</span></li>
                                </li>
                            </ol>
                    
                            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
                        </div>
                    </header>

                    <!-- start: page -->
                    <div class="col-lg-6">
                        <form role="form" method="post" action="{{route('sales.store')}}" enctype="multipart/form-data">
                            {{csrf_field()}}

                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                    <p class="alert alert-danger">{!! $error !!}</p>
                                @endforeach
                            @endif
                            @if (session('status'))
                                <div class="alert alert-success">
                                {{ session('status') }}
                                </div>
                            @endif
                                    <div class="form-group">
                                    <!-- Quote ID needs to be fed from the Quote model, add a third action
                                        button in the view all table? or better handled via the edit quote screen? -->
                                        <label>Quote ID</label>
                                        <input class="form-control" type="text" name="quote_id" id="quote_id" value="{{old('quote_id',$quoteId)}}">
                                    <!-- Client Name to be fed from quotes -->
                                        <label>Client Name</label>
                                        <select class="form-control" name="client_id" id="client_id">
                                            <option value="" ></option>
                                            @foreach ($clients as $client)                        
                                                <option @if($quoteClient && $quoteClient == $client->id) selected @endif address="{{$client->delivery_address}}" value="{!! $client->id !!}">{!! $client->full_name !!}</option>
                                            @endforeach
                                        </select>
                                    <!-- Sales Representative to be fed from quotes -->
                                        <label>Sales Representative</label>
                                        <select class="form-control" name="user_id" id="user_id">
                                                <option value="" ></option>
                                                @foreach ($users as $user)                        
                                                    <option @if($quoteSale && $quoteSale== $user->id) selected @endif value="{!! $user->id !!}">{!! $user->name !!}</option>
                                                @endforeach
                                        </select>
                                        <label>Status</label>
                                        <select class="form-control" name="status" id="status">
                                                <option value="" ></option>
                                                    <option value="in_progress">In Progress</option>                           
                                                    <option value="delivered">Delivered</option>
                                            </select>
                                        <label>Equipment Cost</label>
                                        <input class="form-control" type="number" step="any" name="equipment_cost" id="equipment_cost" value="{{old('equipment_cost')}}">
                                    <!-- Delivery address to be fed from client information -->
                                        <label>Delivery Address</label>
                                        <input class="form-control" type="text" name="delivery_address" id="delivery_address" value="{{old('delivery_address',$quoteClientAdd)}}">
                                        <div class="form-group">
                                            <label>Sale Type</label>
                                            <br>
                                            <label class="checkbox-inline"> <input type="checkbox" value="deploy" name="sale_type[]" id="deploy"> deploy</label>
                                            <label class="checkbox-inline"> <input type="checkbox" value="delivered" name="sale_type[]" id="delivered"> delivered </label>
                                            <label class="checkbox-inline"> <input type="checkbox" value="monitor" name="sale_type[]" id="monitor"> monitor </label>
                                            <label class="checkbox-inline"> <input type="checkbox" value="maintain" name="sale_type[]" id="maintain"> maintain </label>
                                            <label class="checkbox-inline"> <input type="checkbox" value="installation" name="sale_type[]" id="installation"> installation </label>
                                        </div>
                                        <div class="form-group">
                                            <label>Payment Options</label>
                                            <select class="form-control" name="payment_options" id="payment_options">
                                                <option value="" ></option>
                                                <option value="cash">Cash</option>
                                                <option value="50%">50%</option>
                                                <option value="deposit">Deposit</option>
                                                <option value="leasing">Leasing</option>
                                            </select>
                                      </div>

                                        <label>Email Record</label>
                                        <input class="form-control" type="text" name="email_record" id="email_record" value="{{old('email_record')}}">
                                        <label>Sale Date</label>
                                        <input data-plugin-datepicker="" data-plugin-options='{"format": "yyyy/mm/dd"}' class="form-control" name="sale_date" id="sale_date" value="{{old('sale_date')}}">
                                    <!-- Automatically chooses date 28 days after order date -->
                                        <label>Completion &amp; Delivery Date</label>
                                        <input data-plugin-datepicker="" data-plugin-options='{"format": "yyyy/mm/dd"}' class="form-control" name="completion_delivery_date" id="completion_delivery_date" value="{{old('completion_delivery_date')}}" readonly>
                                        <label>Collection / Delivery</label>
                                        <select class="form-control" name="collection_delivery" id="collection_delivery">
                                            <option value="" ></option>
                                            <option value="collection">Collection</option>                           
                                            <option value="delivery">Delivery</option>                          
                                        </select>
                                    <!-- If Delivery is selected then next 3 fields should enable 
                                            'trip_mileage', 'cost_per_mile', 'delivery_cost'
                                            Delivery Cost is calculated: trip mileage * cost per mile -->
                                        <label>Trip Mileage</label>
                                        <input  class="form-control delivery" type="number" step="any" name="trip_mileage" id="trip_mileage" readonly>
                                        <label>Cost Per Mile</label>
                                        <input class="form-control delivery" type="number" step="any" name="cost_per_mile" id="cost_per_mile" readonly>
                                        <label>Deivery Cost</label>
                                        <input class="form-control" type="number" step="any" name="delivery_cost" id="delivery_cost" readonly>
                                        <label>Ral Colour</label>
                                        <input class="form-control" type="text" name="ral_colour" id="ral_colour" value="{{old('ral_colour')}}">


                                    <!-- Here needs an additional field for uploading/handling images to upload -->
                                        <label>Graphics</label>
                                        <input type="file" class="form-control" name="graphics">

                                        <label>Sim Card Received</label>
                                        <select class="form-control" name="sim_card_received" id="sim_card_received">
                                            <option value="" ></option>
                                            <option value="yes">YES</option>                           
                                            <option value="no">NO</option>                         
                                        </select>
                                        <label>Training Required</label>
                                        <select class="form-control" name="training_required" id="training_required">
                                            <option value="" ></option>
                                            <option value="yes">YES</option>                           
                                            <option value="no">NO</option>                         
                                        </select>

                                        <!--If 'YES' is selected from training required then the next 3 fields need to enable -->

                                        <label>Date Booked</label>
                                        <input data-plugin-datepicker="" data-plugin-options='{"format": "yyyy/mm/dd"}' class="form-control training" name="training_date" id="training_date" disabled>
                                        <label>Training Required</label>
                                        <select class="form-control training" name="training_location" id="training_location" disabled>
                                            <option value="" ></option>
                                            <option value="away">Away</option>                           
                                            <option value="workshop">Workshop</option>                         
                                        </select>
                                        <label>Training Cost</label>
                                        <input class="form-control training" type="number" step="any" name="training_cost" id="training_cost" disabled>
                                        <br>
                                    </div>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </form>                           
                    </div>
                    <!-- end: page -->



@endsection

@section('footer')
@include('sales.scripts')
@endsection


