<script>

    $(document).ready(function(){
        $('#client_id').on('change',function(){
            $('#delivery_address').val($('option:selected', this).attr('address'));
        });

        $('#sale_date').on('change',function(){
            var sale_date = $(this).val().split('/');

            var date = new Date(sale_date[0] + '-' + sale_date[1] + '-' + sale_date[2]);
            date.setDate(date.getDate() + 28);
            year = date.getFullYear();
            month = date.getMonth()+1;
            dt = date.getDate();

            if (dt < 10) {
                dt = '0' + dt;
            }
            if (month < 10) {
                month = '0' + month;
            }
            $('#completion_delivery_date').val(year+'/' + month + '/'+dt);

        });


        $('#collection_delivery').on('change',function(){

            var value = $(this).val().toLowerCase();

            if (value == 'delivery')
            {
                $('.delivery').attr('readonly',false);
            }
            else
            {
                $('.delivery').attr('readonly',true);
            }
        });

        $('#trip_mileage').on('change',function(){
            $('#delivery_cost').val($('#cost_per_mile').val() * $(this).val());
        });

        $('#cost_per_mile').on('change',function(){
            $('#delivery_cost').val($('#trip_mileage').val() * $(this).val());
        });

        $('#training_required').on('change',function(){
            var value = $(this).val().toLowerCase();

            if (value == 'yes')
            {
                $('.training').attr('disabled',false);
            }
            else
            {
                $('.training').attr('disabled',true);
            }
        });
    });
</script>