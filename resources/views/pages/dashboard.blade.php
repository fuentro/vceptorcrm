@extends ('layouts.admin')

@section('content')
<div class="inner-wrapper">
                <!-- start: sidebar -->
                <aside id="sidebar-left" class="sidebar-left">
                
                    <div class="sidebar-header">
                        <div class="sidebar-title">
                            Navigation
                        </div>
                        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                        </div>
                    </div>
                
                    <div class="nano">
                        <div class="nano-content">
                            <nav id="menu" class="nav-main" role="navigation">
                            
                                <ul class="nav nav-main">
                                    <li class="nav-active">
                                        <a href="/dashboard">
                                            <i class="fa fa-home" aria-hidden="true"></i>
                                            <span>Dashboard</span>
                                        </a>                        
                                    </li>
                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                            <span>Clients</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="/clients/create">
                                                    Enter New Client
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/clients/all">
                                                    View All Clients
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </li>
                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                            <span>Quotes</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="/quotes/create">
                                                    Enter New Quote
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/quotes/all">
                                                    View All Quotes
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </li>
                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                            <span>Sales</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="/sales/create">
                                                    Enter New Sale
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/sales">
                                                    View All Sales
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </li>
                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="fa fa-id-badge" aria-hidden="true"></i>
                                            <span>Users</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="/users/create">
                                                    Enter New User
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/users">
                                                    View All Users
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </li>                               
                
                        <script>
                            // Maintain Scroll Position
                            if (typeof localStorage !== 'undefined') {
                                if (localStorage.getItem('sidebar-left-position') !== null) {
                                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');
                                    
                                    sidebarLeft.scrollTop = initialPosition;
                                }
                            }
                        </script>
                        
                
                    </div>
                
                </aside>
                <!-- end: sidebar -->

                <section role="main" class="content-body">
                    <header class="page-header">
                        <h2>Dashboard</h2>
                    
                        <div class="right-wrapper pull-right">
                            <ol class="breadcrumbs">
                                <li>
                                    <a href="/dashboard">
                                        <i class="fa fa-home"></i>
                                    </a>
                                </li>
                            </ol>
                    
                            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
                        </div>
                    </header>

                    <!-- start: page -->
                    <!-- Total Sales, Total Clients, Total Users, Sales Conversion In Progress Jobs-->
                    <div class="row">                       
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-md-12 col-lg-6 col-xl-6">
                                    <section class="panel panel-featured-left panel-featured-primary">
                                        <div class="panel-body">
                                            <div class="widget-summary">
                                                <div class="widget-summary-col widget-summary-col-icon">
                                                    <div class="summary-icon bg-primary">
                                                        <i class="fa fa-address-card-o"></i>
                                                    </div>
                                                </div>
                                                <div class="widget-summary-col">
                                                    <div class="summary">
                                                        <h4 class="title">Total Clients</h4>
                                                        <div class="info">
                                                            <strong class="amount">{!! $totalclients !!}</strong>
                                                        </div>
                                                    </div>
                                                    <div class="summary-footer">
                                                        <a class="text-muted text-uppercase" href="/clients/all">(view all)</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-12 col-lg-6 col-xl-6">
                                    <section class="panel panel-featured-left panel-featured-secondary">
                                        <div class="panel-body">
                                            <div class="widget-summary">
                                                <div class="widget-summary-col widget-summary-col-icon">
                                                    <div class="summary-icon bg-secondary">
                                                        <i class="fa fa-bar-chart"></i>
                                                    </div>
                                                </div>
                                                <div class="widget-summary-col">
                                                    <div class="summary">
                                                        <h4 class="title">Sales Conversion</h4>
                                                        <div class="info">
                                                            <strong class="amount">{!! $salesconversion !!}%</strong>
                                                        </div>
                                                    </div>
                                                    <div class="summary-footer">
                                                        <a class="text-muted text-uppercase"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-6 col-xl-6">
                                    <section class="panel panel-featured-left panel-featured-tertiary">
                                        <div class="panel-body">
                                            <div class="widget-summary">
                                                <div class="widget-summary-col widget-summary-col-icon">
                                                    <div class="summary-icon bg-tertiary">
                                                        <i class="fa fa-cart-plus"></i>
                                                    </div>
                                                </div>
                                                <div class="widget-summary-col">
                                                    <div class="summary">
                                                        <h4 class="title">Total Sales</h4>
                                                        <div class="info">
                                                            <strong class="amount">{!! $totalsales !!}</strong>
                                                        </div>
                                                    </div>
                                                    <div class="summary-footer">
                                                        <a class="text-muted text-uppercase" href="/sales/all">(view all)</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-12 col-lg-6 col-xl-6">
                                    <section class="panel panel-featured-left panel-featured-quaternary">
                                        <div class="panel-body">
                                            <div class="widget-summary">
                                                <div class="widget-summary-col widget-summary-col-icon">
                                                    <div class="summary-icon bg-quaternary">
                                                        <i class="fa fa-flag"></i>
                                                    </div>
                                                </div>
                                                <div class="widget-summary-col">
                                                    <div class="summary">
                                                        <h4 class="title">Jobs In Progress</h4>
                                                        <div class="info">
                                                            <strong class="amount">{!! $totalordersinprogress !!}</strong>
                                                        </div>
                                                    </div>
                                                    <div class="summary-footer">
                                                        <a class="text-muted text-uppercase"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>                                
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- end: page -->
                </section>
</div>

@endsection