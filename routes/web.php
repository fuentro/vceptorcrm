<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::group(['middleware' => ['guest']],function(){

    Route::get('/login','Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login','Auth\LoginController@login');
    Route::post('password/email','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/reset','Auth\ResetPasswordController@reset');
    Route::get('password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('password.reset');

});

Route::group(['middleware' => ['auth']],function(){

    Route::post('logout','Auth\LoginController@logout')->name('logout');

    /* Dashboard */
    Route::get('/dashboard', 'PagesController@dashboard');


    /* Clients */
    Route::get('/clients/create', 'ClientsController@create');
    Route::post('clients/create', 'ClientsController@store');
    Route::get('/clients/all', 'ClientsController@index')->name('clients.index');
    Route::get('/clients/{id}/edit', 'ClientsController@edit');
    Route::put('clients/{id}/edit', 'ClientsController@update')->name('clients.update');
    Route::delete('clients/{id}','ClientsController@destroy')->name('clients.destroy');

    /* Quotes */

    Route::get('/quotes/create', 'QuotesController@create');
    Route::post('quotes/create', 'QuotesController@store');
    Route::get('/quotes/all', 'QuotesController@index')->name('quotes.index');
    Route::get('/quotes/{id}/edit', 'QuotesController@edit');
    Route::put('/quotes/{id}/edit', 'QuotesController@update')->name('quotes.update');
    Route::delete('/quotes/{id}','QuotesController@destroy')->name('quotes.destroy');
    Route::get('/quotes/word','QuotesWordController@index')->name('quotes.word');
    Route::get('/quotes/{id}/word','QuotesWordController@show')->name('quotes.show');
    /* Sales */
    Route::resource('sales','SalesController');

    /* Users */
    Route::resource('users','UsersController');
});
