<?php
namespace App\Console;

use App\Mail\SaleReminder;
use Carbon\Carbon;
use App\Sale as SaleModel;
use Mail;

class Sale
{
    public function mail()
    {
        $sales = SaleModel::whereDate('created_at','<=',Carbon::now()->subDays(21)->toDateString())->get();

        foreach ($sales as $sale)
        {
            if(!$sale->mailed)
            {
                Mail::to($sale->email_record)->send(new SaleReminder($sale));
                $sale->update(['mailed' => 1]);
            }

        }
    }
}