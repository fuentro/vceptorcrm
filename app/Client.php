<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['full_name', 'company_name', 'billing_address', 'delivery_same_as_billing', 'delivery_address', 'email', 'telephone_number', 'accounts_contact', 'user_id', 'notes'];
    protected $guarded = ['id'];
}
