<?php

namespace App;

use App\Events\SaleCreated;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = ['quote_id', 'client_id', 'user_id', 'status', 'equipment_cost', 'delivery_address', 'sale_type', 'payment_options', 'email_record', 'sale_date', 'completion_delivery_date', 'collection_delivery', 'trip_mileage',
    	'cost_per_mile', 'delivery_cost', 'ral_colour', 'graphics', 'sim_card_received', 'training_required', 'training_date', 'training_location', 'training_cost','mailed'];
    protected $guarded = ['id'];
    protected $casts = [
        'sale_type' => 'json'
    ];
    protected $events = [
        'saved' => SaleCreated::class,

    ];

    public function graphicsFilePath()
    {
        return public_path('saleImages') .'/' . $this->graphics;
    }

    public function graphicsAssetPath()
    {
        if (empty($this->graphics))
        {
            return null;
        }
        return asset('saleImages') .'/' . $this->graphics;
    }
    public function setGraphicsAttribute($file)
    {
        if (!is_null($this->graphics) && file_exists($this->graphicsFilePath()))
        {
            unlink($this->graphicsFilePath());
        }

        $extension = $file->getClientOriginalExtension();

        $oldFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $fileName = $oldFileName .'-'. str_random(10).'.'.$extension;

        $file->move(public_path('saleImages'),$fileName);

        $this->attributes['graphics'] = $fileName;

    }


}
