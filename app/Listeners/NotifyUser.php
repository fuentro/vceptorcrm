<?php

namespace App\Listeners;

use App\Events\SaleCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Mail\SaleCreated as SaleCreatedMail;

class NotifyUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SaleCreated  $event
     * @return void
     */
    public function handle(SaleCreated $event)
    {
        $sale = $event->sale;

       Mail::to($sale->email_record)->send(new SaleCreatedMail($sale));

    }
}
