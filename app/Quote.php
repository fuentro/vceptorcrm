<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable = ['client_id', 'form_of_enquiry', 'user_id', 'nature_of_sale', 'tower_options', 'other_tower', 'quote_amount', 'quote_date', 'order_placed'];
    protected $guarded = ['id'];

    public function setOrderPlacedAttribute($value)
    {
        $orderedPlace = 0;

        if ( $value === 'on')
        {
            $orderedPlace = 1;
        }

        $this->attributes['order_placed'] = $orderedPlace;
    }

    public function client()
    {
        return $this->belongsTo(Client::class,'client_id');
    }
}
