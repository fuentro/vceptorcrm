<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Client;
use App\Quote;
use App\Sale;

class PagesController extends Controller
{
    public function dashboard(){

    	$totalusers = User::count();
    	$totalclients = Client::count();
    	$totalquotes = Quote::count();
    	$totalquotesconverted = Quote::where('order_placed', 1)->count();
    	$salesconversion = DB::raw($totalquotesconverted / $totalquotes * 100);
    	$totalsales = Sale::count();
    	$totalordersinprogress = Sale::where('status', 'in_progress')->count();

    	return view('pages.dashboard')->with(['totalusers'=>$totalusers, 'totalclients'=>$totalclients, 'totalquotes'=>$totalquotes, 'totalquotesconverted'=>$totalquotesconverted, 'totalsales'=>$totalsales, 'salesconversion'=>$salesconversion, 'totalordersinprogress'=>$totalordersinprogress]);
    }
}
