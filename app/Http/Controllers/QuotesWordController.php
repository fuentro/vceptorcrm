<?php

namespace App\Http\Controllers;

use App\Quote;

class QuotesWordController extends Controller
{
    public function index()
    {
        $quotes = Quote::all();

        if ($quotes->isEmpty())
        {
            return redirect()->route('quotes.index');
        }

        $template = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('template.docx'));

        $firstQuote = $quotes->first();

        $quotesArr = $quotes->toArray();

        $template->cloneRow('arrQuantity', count($quotesArr));

        for($number = 0; $number < $quotes->count(); $number++) {
            $template->setValue('arrQuantity#'.($number+1), htmlspecialchars($quotesArr[$number]['nature_of_sale'], ENT_COMPAT, 'UTF-8'));
            $template->setValue('arrDetails#'.($number+1), htmlspecialchars($quotesArr[$number]['tower_options'], ENT_COMPAT, 'UTF-8'));
            $template->setValue('arrUnitPrice#'.($number+1), htmlspecialchars($quotesArr[$number]['quote_amount'], ENT_COMPAT, 'UTF-8'));
            $template->setValue('arrVat#'.($number+1), htmlspecialchars($quotesArr[$number]['quote_amount'], ENT_COMPAT, 'UTF-8'));
            $template->setValue('arrNet#'.($number+1), htmlspecialchars($quotesArr[$number]['quote_amount'], ENT_COMPAT, 'UTF-8'));
        }

        $template->setValue('quoteId',$firstQuote->id);
        $template->setValue('quoteDate',$firstQuote->quote_date);
        $template->setValue('net',$quotes->sum('quote_amount'));
        $template->setValue('total',$quotes->sum('quote_amount'));

        $template->saveAs(storage_path('quotesReport.docx'));

        return response()->download(storage_path('quotesReport.docx'));
    }

    public function show($quoteId)
    {
        $quote = Quote::findOrFail($quoteId);

        $template = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('template.docx'));

        $template->setValue('arrQuantity', htmlspecialchars($quote->nature_of_sale, ENT_COMPAT, 'UTF-8'));
        $template->setValue('arrDetails', htmlspecialchars($quote->tower_options, ENT_COMPAT, 'UTF-8'));
        $template->setValue('arrUnitPrice', htmlspecialchars($quote->quote_amount, ENT_COMPAT, 'UTF-8'));
        $template->setValue('arrVat', htmlspecialchars($quote->quote_amount, ENT_COMPAT, 'UTF-8'));
        $template->setValue('arrNet', htmlspecialchars($quote->quote_amount, ENT_COMPAT, 'UTF-8'));

        $template->setValue('quoteId',$quote->id);
        $template->setValue('quoteDate',$quote->quote_date);
        $template->setValue('net',$quote->quote_amount);
        $template->setValue('total',$quote->quote_amount);
        $template->setValue('clientName',$quote->client->full_name);
        $template->setValue('clientAdd',$quote->client->delivery_address);

        $template->saveAs(storage_path('singleQuoteReport.docx'));

        return response()->download(storage_path('singleQuoteReport.docx'));

    }
}
