<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Client;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return view('clients.index')->with('clients', $clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();        
        return view('clients.create')->with('users', $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new Client(array(
            'full_name' => $request->get('full_name'), 
            'company_name' => $request->get('company_name'), 
            'billing_address' => $request->get('billing_address'), 
            'delivery_same_as_billing' => $request->get('delivery_same_as_billing'), 
            'delivery_address' => $request->get('delivery_address'), 
            'email' => $request->get('email'), 
            'telephone_number' => $request->get('telephone_number'), 
            'accounts_contact' => $request->get('accounts_contact'), 
            'user_id' => $request->get('user_id'), 
            'notes' => $request->get('notes')
        ));

        $client->save();

        return redirect('/clients/create')->with('status', 'The client has been created!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Client::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)     
    {
        $users = User::all();
        $client = Client::find($id);
        

        return view('clients.edit')->with(['client'=>$client, 'users'=>$users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request,$id) 
    {
       $client = Client::findOrFail($id);

       $client->update($request->all());

        return redirect('/clients/all')->with('status', 'The client has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::where('id',$id)->delete();

        return redirect()->route('clients.index')->with('status','client deleted succesfully');
    }
}
