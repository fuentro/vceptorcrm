<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests\SalesStore;
use App\Quote;
use App\Sale;
use App\User;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sales.index',['sales' => Sale::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $clients = Client::all();
        $users = User::all();
        $quoteId = null;
        $quoteClient = null;
        $quoteSale = null;
        $quoteClientAdd = null;
        
        if ($request->get('quote_id'))
        {
            $quote = Quote::find($request->get('quote_id'));
            $quoteId = $quote->id;
            $quoteClient = $quote->client_id;
            $quoteSale = $quote->user_id;
            $quoteClientAdd = Client::find($quote->client_id)->delivery_address;
        }

        return view('sales.create',compact('clients','users','quoteId','quoteClient','quoteSale','quoteClientAdd'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalesStore $request)
    {
        $data = $request->all();
        try{
            Sale::create($data);
        }
        catch(\Swift_TransportException $e)
        {
            return redirect()->route('sales.index')->withErrors('sale email didn\'t sent but the sale has been created');
        }

        return redirect()->route('sales.index')->with('status','The sale has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sale = Sale::findOrFail($id);
        $clients = Client::all();
        $users = User::all();

        return view('sales.edit',compact('sale','clients','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SalesStore $request, $id)
    {
        $sale = Sale::findOrFail($id);

        $sale->update($request->all());

        return redirect()->route('sales.index')->with('status','The sale has been updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sale::where('id',$id)->delete();

        return redirect()->route('sales.index')->with('status','sale deleted successfully');

    }
}
