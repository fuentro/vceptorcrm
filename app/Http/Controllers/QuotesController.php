<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuoteCreate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Quote;
use App\User;
use App\Client;

class QuotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quotes = Quote::all();
        return view('quotes.index')->with('quotes', $quotes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = DB::table('clients')->select('id','full_name')->get();
        $users = DB::table('users')->select('id','name')->get();               
        return view('quotes.create', ['clients' => $clients], ['users' => $users]);

        return view('quotes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuoteCreate $request)
    {
        $data = $request->all();

        if (!isset($data['order_placed']))
        {
            $data['order_placed'] = 0;
        }

        $quote = Quote::create($data);

        if (isset($data['move_to_sale']) && $data['move_to_sale'] == 1)
        {
           return redirect()->route('sales.create',['quote_id' => $quote->id]);
        }

        return redirect('/quotes/create')->with('status', 'The quote has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::all();
        $clients = Client::all();
        $quote = Quote::find($id);        

        return view('quotes.edit')->with(['clients'=>$clients, 'users'=>$users, 'quote'=>$quote]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $quote = Quote::findOrFail($id);
        $data = $request->all();

        if (!isset($data['order_placed']))
        {
            $data['order_placed'] = 0;
        }

        $quote->update($data);

        return redirect('/quotes/all')->with('status', 'The quote has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Quote::where('id',$id)->delete();

        return redirect()->route('quotes.index')->with('status','quote deleted successfully');
    }
}
