<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalesStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quote_id' =>'required|exists:quotes,id',
            'client_id' => 'required|exists:clients,id',
            'user_id' => 'required|exists:users,id',
            'equipment_cost' => 'required|numeric',
            'delivery_address' => 'required',
            'sale_type' => 'required',
            'payment_options' => 'required',
            'email_record' => 'required|email',
            'sale_date' => 'required',
            'collection_delivery' => 'required',
            'trip_mileage' => 'required_if:collection_delivery,delivery',
            'cost_per_mile' => 'required_if:collection_delivery,delivery',
            'delivery_cost' => 'required_if:collection_delivery,delivery',
            'ral_colour' => 'required',
            'graphics' => 'sometimes|file|image|max:2000',
            'sim_card_received' => 'required',
            'training_required' => 'required',
            'training_date' => 'required_if:training_required,yes',
            'training_location' => 'required_if:training_required,yes',
            'training_cost' => 'required_if:training_required,yes',
            'status' => 'required'
        ];
    }
}
